package com.foo.cafe.item;

import com.foo.cafe.encasement.Packing;

public interface Item {

    public String name();
    public Packing packing();
    public float price();

}
