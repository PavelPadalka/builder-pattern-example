package com.foo.cafe.meal;

import com.foo.cafe.drink.Coke;
import com.foo.cafe.drink.Pepsi;
import com.foo.cafe.food.ChickenBurger;
import com.foo.cafe.food.VegBurger;

public class MealBuilder {

    public Meal prepareVegMeal (){
        Meal meal = new Meal();
        meal.addItem(new VegBurger());
        meal.addItem(new Coke());
        return meal;
    }

    public Meal prepareNonVegMeal (){
        Meal meal = new Meal();
        meal.addItem(new ChickenBurger());
        meal.addItem(new Pepsi());
        return meal;
    }

}