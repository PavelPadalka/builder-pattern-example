package com.foo.cafe.drink;

import com.foo.cafe.encasement.Bottle;
import com.foo.cafe.item.Item;
import com.foo.cafe.encasement.Packing;

/**
 * Created by pavel.padalka on 19.08.2016.
 */
public abstract class ColdDrink implements Item {

    @Override
    public Packing packing() {
        return new Bottle();
    }

    @Override
    public abstract float price();

}
