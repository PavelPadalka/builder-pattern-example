package com.foo.cafe.food;

import com.foo.cafe.item.Item;
import com.foo.cafe.encasement.Packing;
import com.foo.cafe.encasement.Wrapper;

public abstract class Burger implements Item {

    @Override
    public Packing packing() {
        return new Wrapper();
    }

    @Override
    public abstract float price();

}
